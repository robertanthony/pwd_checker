

const chai = require('chai')
const expect = chai.expect

const passwordCheck = require('../index.js')

const tooShort = "A2a"
const noCaps = "a2abcdefghijklmnop"
const noNumber = "Aabcdefghijklmnop"
const good = "A2abcdefghijklmnop"

describe('password checking', () => {
  it('should return false for passwords less than length 10', () => {
    expect(passwordCheck(tooShort)).to.be.false;
  });
  it('should return false for passwords without a capital letter', () => {
    expect(passwordCheck(noCaps)).to.be.false;
  });
  it('should return false for passwords without a number', () => {
    expect(passwordCheck(noNumber)).to.be.false;
  });
  it('should return true for passwords that match criteria', () => {
    expect(passwordCheck(good)).to.be.true;
  });

});
