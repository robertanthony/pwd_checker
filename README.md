
# **Password Checker**

Simple utility to check passwords against a pre-defined internal dict

# **Prepare**

`yarn`


# **Use**

`node password`

# **Test**

`yarn test`


# **Requires**

Node v. 15.x
