
const argv = require('minimist')(process.argv.slice(2));


const digitTester = /\d/g;
const capTester = /[A-Z]/g;

const dict  = {
    length:10,
    must_have_numbers: true,
    must_have_caps: true
}

const passwordCheck =  (password) => {
    if (!password) return false;
    if (typeof password !== "string") return false;
    if (password.length < dict.length) return false; // assumes that 10 is a MINIMUM length
    if (dict.must_have_numbers && !digitTester.test(password)) return false;
    return !(dict.must_have_caps && !capTester.test(password));

}
if (argv._.length) {
   console.log(passwordCheck(argv._[0]))
}

/**
 * alternate version to check a lot of passwords:
 *
 * if (argv._.length) {
 *  for (const pwd of argv_) console.log(passwordCheck(pwd)
 *}
 *
 */


module.exports = passwordCheck;
